from keras.applications.resnet50 import preprocess_input
from keras.applications import ResNet50
from keras.preprocessing.image import load_img, img_to_array
import numpy as np
import os
from tqdm import tqdm
import heapq
import keras.backend as K
from PIL import Image
from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True

anchor = input('Enter query image path:')

# load model
model = ResNet50(include_top = False, weights = 'imagenet', pooling = 'max')

im_a = load_img(anchor, target_size=(224,224))
im_a = img_to_array(im_a)
im_a = np.expand_dims(im_a, axis=0)
im_a = preprocess_input(im_a)

db = '/home/rahul/new_vienna/'
#files = [db+i for i in os.listdir(db)]
with open('trainset.txt','r+') as f:
	im_files = eval(f.read())
files = [db+i for i in im_files]


def euclidean_distance(vects):
	x, y = vects
	return np.sqrt(np.sum((x-y)**2))

'''
for (root,dirs,names) in os.walk(db):
	files += [root+'/'+name for name in names]
'''
print(len(files))

if anchor in set(files):
	files.remove(anchor)

scores = []
cache = {}
for file in tqdm(files):
	try:
		im = load_img(file, target_size=(224,224))
		im = img_to_array(im)
		im = np.expand_dims(im, axis=0)
		im = preprocess_input(im)
	
		feature_a = model.predict(im_a)
		feature_a = feature_a.ravel()
	
		feature_2 = model.predict(im)
		feature_2 = feature_2.ravel()
	
		dist = euclidean_distance([feature_a,feature_2])
		scores = np.append(scores,dist)	
		cache.update({dist:file})
	except ValueError as e:
		print(e)
		print('bad image: ', file)
		pass

ranking = heapq.nsmallest(10, scores)
ranked_files = []
for i in range(len(ranking)):
	ranked_files.append(str((cache[ranking[i]])))

'''
with open('ranked_files.txt','w+') as f:
	for fl in ranked_files:
		f.write(fl+'\n')
'''
montage_euc_def = Image.new('RGB', (2200,200))
x_coord = 0
y_coord = 0
ranked_files.insert(0,anchor)
for k in range(len(ranked_files)):
	im = Image.open(ranked_files[k])
	im.thumbnail((200,200))
	montage_euc_def.paste(im, (x_coord,y_coord))
	x_coord += 200

montage_euc_def.save('/home/rahul/similarity/searchresult/montage_euc_def.png')

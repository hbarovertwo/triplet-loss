import os
from tqdm import tqdm
from sklearn.preprocessing import MultiLabelBinarizer
import numpy as np
import faiss

with open('vienna_cache.txt','r+') as f:
	cache = eval(f.read())

f.close()

with open('new_cond_imgs.txt','r+') as f2:
	imgs = eval(f2.read())

f2.close()

with open('vienna_codes.txt','r+') as f3:
	codes = eval(f3.read())

f3.close()

list_vienna = []

for i in tqdm(range(len(imgs))):
	list_vienna.append(cache[imgs[i]])

print(len(list_vienna))
print(len(imgs))

mlb = MultiLabelBinarizer()
mlb.fit([codes])
print(mlb.classes_)

index = faiss.IndexFlatL2(1552)

for i in tqdm(list_vienna):
	index.add(mlb.transform([i]).astype('float32'))

print(index.ntotal)

db = '/home/rahul/new_vienna/'
triplets = []
for i in tqdm(range(len(imgs))):
	k = 90000
	D, I = index.search(mlb.transform([list_vienna[i]]).astype('float32'),k)
	i_list = I.tolist()[0]
	d_list = D.tolist()[0]
	try:
		i_list.remove(i)
		d_list.remove(0.0)
	except ValueError:
		pass
	if d_list.count(0.0) > 5:
		for j in range(0,5):
			for l in range(0,10):
				if d_list[89998-l] != 0:
					print((imgs[i],imgs[i_list[j]],imgs[i_list[89998-l]]))
					triplets.append((db+imgs[i],db+imgs[i_list[j]],db+imgs[i_list[89998-l]]))
	print(len(triplets))
	if len(triplets) > 2999950:
		break

with open('jacc_trips_3mil_2.txt','w+') as f4:
	f4.write(str(triplets))

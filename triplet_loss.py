# train a triplet loss network on triplets of images (anchor,positive,negative)

import os
import numpy as np
import keras.backend as K

from keras.layers import Input, Flatten, Dense, Lambda, BatchNormalization
from keras.models import Model
from keras.preprocessing.image import ImageDataGenerator
from keras.applications import ResNet50
from keras.applications.resnet50 import preprocess_input
from keras.preprocessing import image
from keras.preprocessing.image import img_to_array
from keras.preprocessing.image import load_img
from keras.optimizers import Nadam
from tqdm import tqdm
from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True
from keras.models import load_model

# load image triplet paths
# triplets = []
db = '/home/rahul/new_vienna/'
with open('jacc_trips_3mil_fix.txt','r+') as f:
	triplets = eval(f.read())

def triplet_loss(y_true, y_pred):
	margin = K.constant(0.5)
	return K.mean(K.maximum(K.constant(0), K.square(y_pred[:,0,0])-K.square(y_pred[:,1,0]) + margin))


def accuracy(y_true, y_pred):
	return K.mean(y_pred[:,0,0] < y_pred[:,1,0])

'''
def l2Norm(x):
	return K.l2_normalize(x,axis=-1)
'''

def euclidean_distance(vects):
	x, y = vects
	return K.sqrt(K.maximum(K.sum(K.square(x-y), axis=1, keepdims=True), K.epsilon()))

resnet_input = Input(shape=(224,224,3))
resnet_model = ResNet50(weights ='imagenet',include_top = False, input_tensor = resnet_input)
resnet_model.layers.pop()

for layer in resnet_model.layers:
	layer.trainable = False

net = resnet_model.output
net = Flatten(name='flatten')(net)
net = Dense(1024, activation='relu', name='embed_1024')(net)
net = BatchNormalization()(net)
net = Dense(128, activation=None, name='embed_128')(net)
#net = Dense(1024, activation='relu', name='embed2_1024')(net)
#net = BatchNormalization()(net)
#net = Dense(128, activation='relu', name='embed2_128')(net)
#net = Dense(1024, activation='relu', name='embed3_1024')(net)
#net = BatchNormalization()(net)
#net = Dense(128, activation='relu', name='embed3_128')(net)
#net = Lambda(l2Norm, output_shape=[128])(net)

base = Model(resnet_model.input, net, name='resnet_model')
base.summary()

input_anchor = Input(shape=(224,224,3), name='input_anchor')
input_positive = Input(shape=(224,224,3), name='input_positive')
input_negative = Input(shape=(224,224,3), name='input_negative')

net_anchor = base(input_anchor)
net_positive = base(input_positive)
net_negative = base(input_negative)

positive_dist = Lambda(euclidean_distance, name='positive_distance')([net_anchor,net_positive])
negative_dist = Lambda(euclidean_distance, name='negative_distance')([net_anchor,net_negative])

stacked_dists = Lambda(
		lambda vects: K.stack(vects, axis=1),
		name='stacked_dists')([positive_dist,negative_dist])


def serve_triplet_batch(triplet_batch):
	dim = len(triplet_batch)
	anchor = np.zeros((dim,224,224,3))
	pos = np.zeros((dim,224,224,3))
	neg = np.zeros((dim,224,224,3))

	for n,triplet in enumerate(triplet_batch):
		im_a = load_img(triplet[0],target_size=(224,224))
		im_a = img_to_array(im_a)
		im_a = np.expand_dims(im_a, axis=0)
		im_a = preprocess_input(im_a)
		anchor[n] = im_a

		im_p = load_img(triplet[1], target_size=(224,224))
		im_p = img_to_array(im_p)
		im_p = np.expand_dims(im_p, axis=0)
		im_p = preprocess_input(im_p)
		pos[n] = im_p

		im_n = load_img(triplet[2], target_size=(224,224))
		im_n = img_to_array(im_n)
		im_n = np.expand_dims(im_n, axis=0)
		im_n = preprocess_input(im_n)
		neg[n] = im_n
		
	Y_train = np.random.randint(2, size=(1,2,dim)).T
	return anchor, pos, neg, Y_train


def image_triplet_generator(image_triples, batch_size):
	while True:
		num_triplets = len(image_triples)
		indices = np.random.permutation(np.arange(num_triplets))
		num_batches = num_triplets // batch_size
		for b in range(num_batches):
			batch_indices = indices[b*batch_size : (b+1)*batch_size]
			triplet_list = [image_triples[i] for i in batch_indices]
			a, p, n, y = serve_triplet_batch(triplet_list)
			yield [a,p,n],y

#if from scratch, uncomment below
model = Model([input_anchor, input_positive, input_negative], stacked_dists, name='triplet_siamese')

opt = Nadam()
model.compile(optimizer=opt, loss=triplet_loss, metrics=[accuracy])

BS = 165
# model.fit([anchor, pos, neg],Y_train, epochs=5, batch_size=15)

gen = image_triplet_generator(triplets, BS)

model.fit_generator(gen,
		     steps_per_epoch = 17958,
		     epochs = 3)

model.save('./h5files/triplet_loss_vienna_3mil.h5')

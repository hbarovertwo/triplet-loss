from keras.applications.resnet50 import preprocess_input
from keras.preprocessing.image import load_img, img_to_array
import numpy as np
import os
from tqdm import tqdm
import heapq
import keras.backend as K
from PIL import Image, ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True

anchor = input('Enter query image path:')

from keras.models import load_model

def triplet_loss(y_true, y_pred):
	margin = K.constant(0.5)
	return K.mean(K.maximum(K.constant(0), K.square(y_pred[:,0,0])-K.square(y_pred[:,1,0]) + margin))

# load model
model = load_model('./h5files/triplet_loss_vienna_3mil.h5', custom_objects={'triplet_loss':triplet_loss})


#model.summary()

im_a = load_img(anchor, target_size=(224,224))
im_a = img_to_array(im_a)
im_a = np.expand_dims(im_a, axis=0)
im_a = preprocess_input(im_a)

db = '/home/rahul/new_vienna/'
with open('trainset.txt','r+') as f:
	im_files = eval(f.read())
files = [db+i for i in im_files]

print(len(files))

if anchor in set(files):
	files.remove(anchor)

scores = []
cache = {}
for file in tqdm(files):
	try:
		im = load_img(file, target_size=(224,224))
		im = img_to_array(im)
		im = np.expand_dims(im, axis=0)
		im = preprocess_input(im)
		
		pred = model.predict([im_a,im,im_a])
		scores.append(pred[0][0][0])
		cache.update({pred[0][0][0]:file})
	except ValueError as e:
		print(e)
		print("ValueError on image: ", file)

print(len(scores))
ranking = heapq.nsmallest(10, scores)
ranked_files = []
for i in range(len(ranking)):
	ranked_files.append(str((cache[ranking[i]])))

with open('ranked_files.txt','w+') as f2:
	for fl in ranked_files:
		f2.write(fl+'\n')

montage = Image.new('RGB', (2200,200))
x_coord = 0
y_coord = 0
ranked_files.insert(0,anchor)
for k in range(len(ranked_files)):
	im = Image.open(ranked_files[k])
	im.thumbnail((200,200))
	montage.paste(im, (x_coord,y_coord))
	x_coord += 200

montage.save('/home/rahul/similarity/searchresult/montage.png')
